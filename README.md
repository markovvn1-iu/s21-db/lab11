# Lab 11

### Exercise 1.1

```
Create
(p1:Fighter {name: "Brandon Vera", wight: "205"}),
(p2:Fighter {name: "Brock Lesnar", wight: "230"}),
(p3:Fighter {name: "Daniel Cormier", wight: "205"}),
(p4:Fighter {name: "Frank Mir", wight: "230"}),
(p5:Fighter {name: "Jon Jones", wight: "205"}),
(p6:Fighter {name: "Kelvin Gastelum", wight: "185"}),
(p7:Fighter {name: "Khabib Nurmagomedov", wight: "155"}),
(p8:Fighter {name: "Matt Hamill", wight: "185"}),
(p9:Fighter {name: "Michael Bisping", wight: "185"}),
(p10:Fighter {name: "Neil Magny", wight: "170"}),
(p11:Fighter {name: "Rafael Dos Anjos", wight: "155"}),
(p7)-[:beats]->(p11),
(p11)-[:beats]->(p10),
(p5)-[:beats]->(p3),
(p9)-[:beats]->(p8),
(p5)-[:beats]->(p1),
(p1)-[:beats]->(p4),
(p4)-[:beats]->(p2),
(p10)-[:beats]->(p6),
(p6)-[:beats]->(p9),
(p9)-[:beats]->(p8),
(p9)-[:beats]->(p6),
(p8)-[:beats]->(p5)
```

## Exercise 1.2

Return all **middle/Walter/light** weight fighters (155,170,185) who at least have one win.

```
Match (a:Fighter)-[:beats]->(b:Fighter)
with a, b, count(a) as cnt
where a.wight in ["155", "170", "185"] and cnt > 0 return a;
```

![](.gitlab/ex2.1.png)

Return fighters who had **1-1 record** with each other. Use **Count** from the aggregation functions.

```
Match (a:Fighter)-[:beats]->(b:Fighter), (b:Fighter)-[:beats]->(a:Fighter) return a;
```

![](.gitlab/ex2.2.png)

Return all fighter that can "**Khabib Nurmagomedov**" beat them and he didn’t have a fight with them yet.

```

```

Return **undefeated Fighters(0 loss)**, **defeated fighter (0 wins)**.

```
Match (a:Fighter) where not (a:Fighter)-[:beats]->(:Fighter) return a;
Match (a:Fighter) where not (a:Fighter)<-[:beats]-(:Fighter) return a;
```

![](.gitlab/ex2.4.1.png)
![](.gitlab/ex2.4.2.png)